<?php
//Casos
use App\Http\Controllers\CasosController;
//Ia14
use App\Http\Controllers\Ia14Controller;
//ia7
use App\Http\Controllers\Ia7Controller;
//Muertos
use App\Http\Controllers\MuertosController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//IA14
Route::resource('Ia14',Ia14Controller::class);
//Show
Route::get('Ia14/{fecha}',[Ia14Controller::class,'show']);
Route::get('/Ia14all',[Ia14Controller::class,'showAll']);
//Add
Route::post('/Ia14add',[Ia14Controller::class,'store']);
//Update
Route::patch('/Ia14update',[Ia14Controller::class,'update']);
//Delete
Route::delete('/Ia14del/{fecha}',[Ia14Controller::class,'delete']);


//Coleccion:
Route::get('Ia14/{fecha}/{fecha2}',[Ia14Controller::class,'showCollection']);


//IA7
Route::resource('Ia7',Ia7Controller::class);
//Show
Route::get('Ia7/{fecha}',[Ia7Controller::class,'show']);
Route::get('/Ia7all',[Ia7Controller::class,'showAll']);
//add
Route::post('/Ia7add',[Ia7Controller::class,'store']);
//Update
Route::patch('/Ia7update',[Ia7Controller::class,'update']);
//Delete
Route::delete('/Ia7del/{fecha}',[Ia7Controller::class,'delete']);

//Coleccion:
Route::get('Ia7/{fecha}/{fecha2}',[Ia7Controller::class,'showCollection']);

//Muertos
Route::resource('Muertos',MuertosController::class);
//Show
Route::get('Muertos/{fecha}',[MuertosController::class,'show']);
Route::get('/Muertosall',[MuertosController::class,'showAll']);
//add
Route::post('/Muertosadd',[MuertosController::class,'store']);
//Update
Route::patch('/Muertosupdate',[MuertosController::class,'update']);
//Delete
Route::delete('/Muertosdel/{fecha}',[MuertosController::class,'delete']);
//Coleccion:
Route::get('Muertos/{fecha}/{fecha2}',[MuertosController::class,'showCollection']);

//Casos
Route::resource('Casos',CasosController::class);
//Show
Route::get('Casos/{fecha}',[CasosController::class,'show']);
Route::get('/Casosall',[CasosController::class,'showAll']);
//Add
Route::post('/Casosadd',[CasosController::class,'store']);
//Update
Route::patch('/Casosupdate',[CasosController::class,'update']);
//Delete
Route::delete('/Casosdel/{fecha}',[CasosController::class,'delete']);
//Coleccion:
Route::get('Casos/{fecha}/{fecha2}',[CasosController::class,'showCollection']);
