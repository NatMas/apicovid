<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CCAAsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ccaas')->insert([
            'nombre'=>'Catalunya',
            'pais_id'=> 1,
        ]);
    }
}
