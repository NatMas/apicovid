<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ia7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ia7',function (Blueprint $table){
            $table->id();
            $table->date('fecha');
            $table->integer('ccaas_id');
            $table->decimal('incidencia',8,2);
            $table->foreign("ccaas_id")->references('id')->on('ccaas')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ia7',function (Blueprint $table){

            $table->dropColumn('fecha');
            $table->dropColumn('ccaas_id');
            $table->dropColumn('incidencia');

        });
    }
}
