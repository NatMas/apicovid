<?php

namespace App\Http\Resources;

use App\Models\CCAAs;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class ShowResouceMuertosCasos extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $pais = DB::table('paises')
            ->join('ccaas', 'paises.id', '=', 'ccaas.pais_id')
            ->select('paises.*')
            ->first();
        $ccaa = ccaas::where('id',$this->ccaas_id)->first();
        return [
            'fecha'=>$this->fecha,
            'pais'=>$pais->nombre,
            'ccaa'=>$ccaa->nombre,
            'value'=>$this -> numero
        ];
    }
}
