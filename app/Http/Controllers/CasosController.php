<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\CovidCollectionMuertosCasos;
use App\Http\Resources\ShowResouceMuertosCasos;
use App\Models\casos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CasosController extends Controller
{
    public function show($id){
        //http://localhost:8000/api/casos/2021-02-09
        $casos = casos::where('fecha',$id)->first();
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return new ShowResouceMuertosCasos($casos);
    }

    public function showAll(){
        $casos =casos::all();

        if (!$casos){
            return response()-> json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return new CovidCollectionMuertosCasos($casos);
    }

    public function store(Request $request){
        //Nos aseguramos que esta puesto el POST en Postman
        //http://localhost:8000/api/casosadd?fecha=2021-02-10&ccaas_id=1&incidencia=391.00

        $casos = new casos();
        $casos ->fecha = $request->fecha;
        $casos ->ccaas_id = $request->ccaas_id;
        $casos ->numero = $request->numero;
        $casos->save();
        return response()->json($casos);
    }

    public function update(Request $request){
        //Nos aseguramos que esta puesto el PATCH en Postman
        //Probar update:
        //http://localhost:8000/api/casosupdate?fecha=2021-02-09&ccaas_id=1&incidencia=160
        //Mostrar update:
        //http://localhost:8000/api/casos/2021-02-09
        $casos = casos::where('fecha',$request->fecha)->first();
        $casos ->fecha = $request->fecha;
        $casos ->ccaas_id = $request->ccaas_id;
        $casos ->numero = $request->numero;
        $casos->save();
        return response()->json($casos);
    }

    public function delete($id){
        //Nos aseguramos que esta puesto el DELETE en Postman
        //Eliminar:
        //http://localhost:8000/api/casosdel/2021-02-10
        //Comprobar eliminado:
        //http://localhost:8000/api/casos/2021-02-10
        $casos = casos::where('fecha',$id)->first();
        if ($casos){
            $casos ->delete();
        }else{
            return response()->json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return response()->json(null);
    }

    public function showCollection($id,$id2){
        if ($id>$id2){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Primera fecha mayor que la segunda.'])],404);

        }
        $casos = DB::select(DB::raw("SELECT * FROM casos WHERE fecha BETWEEN '$id' and '$id2'"));
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return new CovidCollection($casos);
    }
}
