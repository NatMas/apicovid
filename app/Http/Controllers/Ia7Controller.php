<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\ia7;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ia7Controller extends Controller
{
    public function show($id){
        //http://localhost:8000/api/Ia7/2021-02-09
        $ia7 = ia7::where('fecha',$id)->first();
        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return new ShowResource($ia7);
    }

    public function showAll(){
        $ia7 =ia7::all();

        if (!$ia7){
            return response()-> json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return new CovidCollection($ia7);
    }

    public function store(Request $request){
        //Nos aseguramos que esta puesto el POST en Postman
        //http://localhost:8000/api/Ia7add?fecha=2021-02-10&ccaas_id=1&incidencia=391.00

        $ia7 = new ia7();
        $ia7 ->fecha = $request->fecha;
        $ia7 ->ccaas_id = $request->ccaas_id;
        $ia7 ->incidencia = $request->incidencia;
        $ia7->save();
        return response()->json($ia7);
    }

    public function update(Request $request){
        //Nos aseguramos que esta puesto el PATCH en Postman
        //Probar update:
        //http://localhost:8000/api/Ia7update?fecha=2021-02-09&ccaas_id=1&incidencia=160
        //Mostrar update:
        //http://localhost:8000/api/Ia7/2021-02-09
        $ia7 = ia7::where('fecha',$request->fecha)->first();
        $ia7 ->fecha = $request->fecha;
        $ia7 ->ccaas_id = $request->ccaas_id;
        $ia7 ->incidencia = $request->incidencia;
        $ia7->save();
        return response()->json($ia7);
    }

    public function delete($id){
        //Nos aseguramos que esta puesto el DELETE en Postman
        //Eliminar:
        //http://localhost:8000/api/Ia7del/2021-02-10
        //Comprobar eliminado:
        //http://localhost:8000/api/Ia7/2021-02-10
        $ia7 = ia7::where('fecha',$id)->first();
        if ($ia7){
            $ia7 ->delete();
        }else{
            return response()->json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return response()->json(null);
    }

    public function showCollection($id,$id2){
        if ($id>$id2){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Primera fecha mayor que la segunda.'])],404);

        }
        $ia7 = DB::select(DB::raw("SELECT * FROM ia7 WHERE fecha BETWEEN '$id' and '$id2'"));
        if (! $ia7)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return new CovidCollection($ia7);
    }
}
