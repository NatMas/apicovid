<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\ShowResource;
use App\Models\ia14;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ia14Controller extends Controller
{

    public function show($id){
        //http://localhost:8000/api/Ia14/2021-02-09
        $ia14 = ia14::where('fecha',$id)->first();
        if (! $ia14)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return new ShowResource($ia14);
    }

    public function showAll(){
       $ia14 =ia14::all();

        if (!$ia14){
            return response()-> json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return new CovidCollection($ia14);
    }

    public function store(Request $request){
        //Nos aseguramos que esta puesto el POST en Postman
        //http://localhost:8000/api/Ia14add?fecha=2021-02-10&ccaas_id=1&incidencia=391.00

        $ia14 = new Ia14();
        $ia14 ->fecha = $request->fecha;
        $ia14 ->ccaas_id = $request->ccaas_id;
        $ia14 ->incidencia = $request->incidencia;
        $ia14->save();
        return response()->json($ia14);
    }

    public function update(Request $request){
        //Nos aseguramos que esta puesto el PATCH en Postman
        //Probar update:
        //http://localhost:8000/api/Ia14update?fecha=2021-02-09&ccaas_id=1&incidencia=160
        //Mostrar update:
        //http://localhost:8000/api/Ia14/2021-02-09
        $ia14 = ia14::where('fecha',$request->fecha)->first();
        $ia14 ->fecha = $request->fecha;
        $ia14 ->ccaas_id = $request->ccaas_id;
        $ia14 ->incidencia = $request->incidencia;
        $ia14->save();
        return response()->json($ia14);
    }

    public function delete($id){
        //Nos aseguramos que esta puesto el DELETE en Postman
        //Eliminar:
        //http://localhost:8000/api/Ia14del/2021-02-10
        //Comprobar eliminado:
        //http://localhost:8000/api/Ia14/2021-02-10
        $ia14 = ia14::where('fecha',$id)->first();
        if ($ia14){
            $ia14 ->delete();
        }else{
            return response()->json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return response()->json(null);
    }

    public function showCollection($id,$id2){
        if ($id>$id2){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Primera fecha mayor que la segunda.'])],404);

        }
        //http://127.0.0.1:8000/api/Ia14/2021-02-09/2021-02-19
        $ia14 = DB::select(DB::raw("SELECT * FROM ia14 WHERE fecha BETWEEN '$id' and '$id2'"));
        if (! $ia14)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return new CovidCollection($ia14);
    }
}

