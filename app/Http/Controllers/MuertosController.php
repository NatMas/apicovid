<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Http\Resources\CovidCollectionMuertosCasos;
use App\Http\Resources\ShowResouceMuertosCasos;
use App\Models\muertos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MuertosController extends Controller
{
    public function show($id){
        //http://localhost:8000/api/Muertos/2021-02-09
        $Muertos = Muertos::where('fecha',$id)->first();
        if (! $Muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return new ShowResouceMuertosCasos($Muertos);
    }

    public function showAll(){
        $Muertos =Muertos::all();

        if (!$Muertos){
            return response()-> json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return new CovidCollectionMuertosCasos($Muertos);
    }

    public function store(Request $request){
        //Nos aseguramos que esta puesto el POST en Postman
        //http://localhost:8000/api/Muertosadd?fecha=2021-02-10&ccaas_id=1&incidencia=391.00

        $Muertos = new Muertos();
        $Muertos ->fecha = $request->fecha;
        $Muertos ->ccaas_id = $request->ccaas_id;
        $Muertos ->numero = $request->numero;
        $Muertos->save();
        return response()->json($Muertos);
    }

    public function update(Request $request){
        //Nos aseguramos que esta puesto el PATCH en Postman
        //Probar update:
        //http://localhost:8000/api/Muertosupdate?fecha=2021-02-09&ccaas_id=1&incidencia=160
        //Mostrar update:
        //http://localhost:8000/api/Muertos/2021-02-09
        $Muertos = Muertos::where('fecha',$request->fecha)->first();
        $Muertos ->fecha = $request->fecha;
        $Muertos ->ccaas_id = $request->ccaas_id;
        $Muertos ->numero = $request->numero;
        $Muertos->save();
        return response()->json($Muertos);
    }

    public function delete($id){
        //Nos aseguramos que esta puesto el DELETE en Postman
        //Eliminar:
        //http://localhost:8000/api/Muertosdel/2021-02-10
        //Comprobar eliminado:
        //http://localhost:8000/api/Muertos/2021-02-10
        $Muertos = Muertos::where('fecha',$id)->first();
        if ($Muertos){
            $Muertos ->delete();
        }else{
            return response()->json(['errors'=> Array(['code'=>404,'message'=>'No hay campos'])]);
        }
        return response()->json(null);
    }

    public function showCollection($id,$id2){
        if ($id>$id2){
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'Primera fecha mayor que la segunda.'])],404);

        }
        $Muertos = DB::select(DB::raw("SELECT * FROM Muertos WHERE fecha BETWEEN '$id' and '$id2'"));
        if (! $Muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra esta fecha.'])],404);
        }
        return new CovidCollection($Muertos);
    }
}
